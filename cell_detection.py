### Author: Li Wen Lin
### This is for auto crop and area calculation of cell-shaped images
### Inspired by https://www.mathworks.com/help/images/detecting-a-cell-using-image-segmentation.html

from os import path, listdir
import cv2
import numpy as np

def apply_brightness_contrast(input_img, brightness = 0, contrast = 0):

    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow

        buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()

    if contrast != 0:
        f = 131*(contrast + 127)/(127*(131-contrast))
        alpha_c = f
        gamma_c = 127*(1-f)

        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf

### Do mode 1 first, if result not valid (bad result) then try mode 2
### mode 1: segemetation -> erosion -> floodfill -> dilation -> smooth
### mode 2: add contrast -> erosion -> floodfill -> dilation -> smooth
def detection(img, mode):

    if mode == 1: ### add brightness and contrast
        ret, pre_img = cv2.threshold(img,255,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    elif mode == 2: ### image segmentation
        pre_img = apply_brightness_contrast(img, 50, 135)

    ### Erosion
    kernel = np.ones((2, 2), np.uint8)
    erode_img = cv2.erode(pre_img, kernel)

    ### flood fill
    H, W = img.shape
    mask = np.zeros((H+2, W+2), np.uint8)
    fill_img = erode_img.copy()

    ## assume cell-to-detect is in the center
    cv2.floodFill(fill_img, mask, (round(H/2), round(W/2)), 100)

    ### focus to cell
    cell_img = fill_img.copy()
    for i in range(H):
        for j in range(W):
            if cell_img[i, j] != 100:
                cell_img[i, j] = 0
            else:
                cell_img[i, j] = 255

    ### dilation
    kernel = np.ones((10, 10), np.uint8)
    dilate_img = cv2.dilate(cell_img, kernel)

    ### clean border
    cleanBor_img = cv2.copyMakeBorder(dilate_img, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[255])
    e_H, e_W = cleanBor_img.shape
    e_mask = np.zeros((e_H+2, e_W+2), np.uint8)
    cv2.floodFill(cleanBor_img, e_mask, (5, 5), (0),flags=cv2.FLOODFILL_FIXED_RANGE)

    ### smooth
    smooth_img = cv2.GaussianBlur(cleanBor_img, (15, 15), 0)

    ### change mask color
    final_mask = np.zeros((H, W, 4), np.uint8)
    area = 0
    for i in range(H):
        for j in range(W):
            if smooth_img[i, j] != 0:
                final_mask[i, j, :] = (0, 255, 0, 80)
                area += 1
            else:
                final_mask[i, j, :] = (0, 0, 0, 0)
                
    ### change original image to RGB channel
    img_rgb = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    ### overlay image
    overlay_img = cv2.addWeighted(img_rgb, 1, final_mask[:, :, 0:3], 0.25, 0)

    ### calcultate area
    area_percentage = area/(H*W)

    return area_percentage, overlay_img, final_mask