from PyQt5 import QtCore, QtGui, QtWidgets, Qt

class Ui_PainterWindow(QtWidgets.QMainWindow):
    def __init__(self, bgPath, maskPath, H, W):
        super().__init__()
        top, left = 400, 400
        self.W, self.H = W, H

        self.setWindowTitle("Mask Editor")
        self.setWindowIcon(QtGui.QIcon("assets/icon/cell.png"))
        self.setGeometry(top, left, self.W, self.H)

        self.image = QtGui.QImage(self.size(), QtGui.QImage.Format_ARGB32)
        self.image.fill(QtCore.Qt.transparent)
        self.image.load(bgPath)
        self.imageDraw = QtGui.QImage(self.size(), QtGui.QImage.Format_ARGB32)
        self.imageDraw.fill(QtCore.Qt.transparent)
        self.imageDraw.load(maskPath)

        self.drawing = False
        self.brushSize = 20
        self._clear_size = 20
        self.brushColor = QtGui.QColor(0, 255, 0, 30)
        self.lastPoint = QtCore.QPoint()

        self.change = False
        mainMenu = self.menuBar()

        self.savePath = maskPath
        save = mainMenu.addMenu("File")
        self.saveAction = QtWidgets.QAction("Save and Exit", self)
        self.saveAction.setIcon(QtGui.QIcon("assets/icon/save.png"))
        self.saveAction.setShortcut("Ctrl+S")
        save.addAction(self.saveAction)
        self.saveAction.triggered.connect(self.saveExit)

        tool = mainMenu.addMenu("Tool")
        self.changeAction = QtWidgets.QAction("Eraser", self)
        self.changeAction.setIcon(QtGui.QIcon("assets/icon/eraser.png"))
        self.changeAction.setShortcut("Ctrl+A")
        tool.addAction(self.changeAction)
        self.changeAction.triggered.connect(self.changeColour)

        self.scaleUpAction = QtWidgets.QAction("Scale up", self)
        self.scaleUpAction.setIcon(QtGui.QIcon("assets/icon/plus.png"))
        self.scaleUpAction.setShortcut("Up")
        tool.addAction(self.scaleUpAction)
        self.scaleUpAction.triggered.connect(self.scaleUp)
        self.scaleDownAction = QtWidgets.QAction("Scale Down", self)
        self.scaleDownAction.setIcon(QtGui.QIcon("assets/icon/minus.png"))
        self.scaleDownAction.setShortcut("Down")
        tool.addAction(self.scaleDownAction)
        self.scaleDownAction.triggered.connect(self.scaleDown)

        self.clearAction = QtWidgets.QAction("Clear All", self)
        self.clearAction.setIcon(QtGui.QIcon("assets/icon/refresh.png"))
        self.clearAction.setShortcut("Ctrl+C")
        tool.addAction(self.clearAction)
        self.clearAction.triggered.connect(self.clear)


    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.drawing = True
            self.lastPoint = event.pos()

    def mouseMoveEvent(self, event):
        if event.buttons() and QtCore.Qt.LeftButton and self.drawing:
            painter = QtGui.QPainter(self.imageDraw)
            painter.setPen(QtGui.QPen(self.brushColor, self.brushSize, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
            if self.change:
                r = QtCore.QRect(QtCore.QPoint(), self._clear_size*QtCore.QSize())
                r.moveCenter(event.pos())
                painter.save()
                painter.setCompositionMode(QtGui.QPainter.CompositionMode_Clear)
                painter.eraseRect(r)
                painter.restore()
            else:
                painter.drawLine(self.lastPoint, event.pos())
            painter.end()
            self.lastPoint = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button == QtCore.Qt.LeftButton:
            self.drawing = False

    def paintEvent(self, event):
        canvasPainter = QtGui.QPainter(self)
        canvasPainter.drawImage(self.rect(), self.image, self.image.rect())
        canvasPainter.drawImage(self.rect(), self.imageDraw, self.imageDraw.rect())

    def scaleUp(self):
        if self.brushSize == 1:
            self.brushSize = 10
            self._clear_size = 10
        else:
            self.brushSize += 10
            self._clear_size += 10

        if self.change:
            pixmap = QtGui.QPixmap(QtCore.QSize(1, 1)*self._clear_size)
            pixmap.fill(QtCore.Qt.transparent)
            painter = QtGui.QPainter(pixmap)
            painter.setPen(QtGui.QPen(QtCore.Qt.black, 2))
            painter.drawRect(pixmap.rect())
            painter.end()
            cursor = QtGui.QCursor(pixmap)
            QtWidgets.QApplication.changeOverrideCursor(cursor)

    def scaleDown(self):
        self.brushSize -= 10
        self._clear_size -= 10

        if self.brushSize <= 1:
            self.brushSize = 1
            self._clear_size = 1

        if self.change:
            pixmap = QtGui.QPixmap(QtCore.QSize(1, 1)*self._clear_size)
            pixmap.fill(QtCore.Qt.transparent)
            painter = QtGui.QPainter(pixmap)
            painter.setPen(QtGui.QPen(QtCore.Qt.black, 2))
            painter.drawRect(pixmap.rect())
            painter.end()
            cursor = QtGui.QCursor(pixmap)
            QtWidgets.QApplication.changeOverrideCursor(cursor)
    
    def changeColour(self):
        self.change = not self.change
        if self.change:
            self.changeAction.setText("Brush")
            self.changeAction.setIcon(QtGui.QIcon("assets/icon/brush.png"))
            pixmap = QtGui.QPixmap(QtCore.QSize(1, 1)*self._clear_size)
            pixmap.fill(QtCore.Qt.transparent)
            painter = QtGui.QPainter(pixmap)
            painter.setPen(QtGui.QPen(QtCore.Qt.black, 2))
            painter.drawRect(pixmap.rect())
            painter.end()
            cursor = QtGui.QCursor(pixmap)
            QtWidgets.QApplication.setOverrideCursor(cursor)
        else:
            self.changeAction.setText("Eraser")
            self.changeAction.setIcon(QtGui.QIcon("assets/icon/eraser.png"))
            QtWidgets.QApplication.restoreOverrideCursor()
    
    def saveExit(self):
        self.imageDraw = self.imageDraw.scaled(self.W, self.H, Qt.Qt.IgnoreAspectRatio, Qt.Qt.FastTransformation)
        self.imageDraw.save(self.savePath)
        QtWidgets.QApplication.restoreOverrideCursor()
        self.close()

    def clear(self):
        self.imageDraw.fill(QtCore.Qt.transparent)
        self.update()
    
    def closeEvent(self, a0):
        QtWidgets.QApplication.restoreOverrideCursor()
        return super().closeEvent(a0)

    def resizeEvent(self, a0):
        self.imageDraw = self.imageDraw.scaled(self.size(), Qt.Qt.IgnoreAspectRatio, Qt.Qt.FastTransformation)
        return super().resizeEvent(a0)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PainterWindow = Ui_PainterWindow("assets/empty_bg.png", "assets/empty_bg.png", 512, 512)
    PainterWindow.show()
    sys.exit(app.exec_())